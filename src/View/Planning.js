import React, {useState} from 'react';
import axios from "axios";
import getDomain from "../utils";
import ModuleList from "../Components/ModuleList";
import HCaptcha from "@hcaptcha/react-hcaptcha";
import config from "../config.json";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import {Redirect} from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Container from "@material-ui/core/Container";

const useStyles = makeStyles((theme) => ({
    formUnit: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
    },
    card: {
        minWidth: 300
    },
}));


function Planning() {
    const classes = useStyles();

    const [emailValue, setEmailValue] = useState("");
    const [emailSearched, setEmailSearched] = useState("");

    const [found, setFound] = useState(false);
    const [num, setNum] = useState("");
    const [modules, setModules] = useState([])
    const [modif, setModif] = useState(false)

    const [erreur, setErreur] = useState("")
    const [succes, setSucces] = useState(false);

    const searchMail = () => {
        axios
            .get(getDomain() + '/api/search', { params: {mail: emailValue, num: num}})
            .then(res => {
                let mod = [];
                for (let i=0;i<res.data.length;i++) {
                    mod.push(res.data[i].pk);
                }
                console.log(mod);
                setModules(mod);
                setFound(true);
                setEmailSearched(emailValue);
            })
            .catch(err => {
                console.log(err);
                setFound(false)
                setModules([]);
                setEmailSearched("")
            });
    };

    const changeMail = event => {
        setEmailValue(event.target.value);
    }

    const changeNum = event => {
        setNum(event.target.value);
    }

    const modifModules = () => {
        setModif(true);
        return false;
    }

    const formSubmit = (e) => {
        e.preventDefault();

        let formData = new FormData(document.querySelector('form'))

        axios
            .post((config.DEBUG ? config.URL_DEV : config.URL_PROD) + '/api/reservation/update/',
                formData)
            .then((result) => {
                console.log(result.status);
                console.log(result.data);
                console.log("Succès !")
                setSucces(true);
                return  <Redirect to="/success/" />
            })
            .catch((err) => {
                console.log("erreur")
                console.log(err);
                setErreur(JSON.stringify(err.response.data));
            });
    }

    return (
        <React.Fragment>
            {succes && <Redirect to={'/success/'}/>}
            <h1>Voir mes modules</h1>

            <Container maxWidth="md">
            <Card className={classes.card} variant="outlined">
                <CardContent>
                    <form onSubmit={formSubmit}>
                        <div className={classes.formUnit}>
                            <FormControl component="fieldset">
                                <FormControlLabel
                                    control={<TextField variant="filled"
                                                        name="email"
                                                        label="mail"
                                                        size="small"
                                                        onChange={changeMail}
                                    />}
                                    label="Mail"
                                    labelPlacement="start"/>
                            </FormControl>

                            <FormControl component="fieldset">
                                <FormControlLabel
                                    control={<TextField variant="filled"
                                                        name="num_etudiant"
                                                        required={true}
                                                        label="0401..."
                                                        size="small"
                                                        onChange={changeNum}
                                    />}
                                    label="Numéro étudiant"
                                    labelPlacement="start"/>
                            </FormControl>
                        </div>

                        <div className={classes.formUnit}>
                            <Button variant="contained" color="primary" type="button" onClick={()=> searchMail()}>Chercher</Button>
                        </div>

                        { found ?
                            <>

                            <div className={classes.formUnit}>
                                <Typography variant="h5">Modules pour {emailSearched}</Typography>
                                <Button variant="contained" type="button" onClick={()=> modifModules()}>Modifier</Button>
                            </div>

                                {erreur && <Alert severity="error">{erreur}</Alert>}

                            <div className={classes.formUnit}>
                                <ModuleList selected={modules} modif={modif}/>

                                {modif && <HCaptcha sitekey={config.HCAPTCHA_PUBLIC_KEY}/>}
                                {modif && <input type="submit" value="Submit"/>}
                            </div>
                            </>
                        : <div>Aucun résultat trouvé, ou numéro étudiant associé incorrect</div> }
                    </form>
                </CardContent>
            </Card>
            </Container>
        </React.Fragment>
    );
}

export default Planning;