import React, {useState} from 'react';
import HCaptcha from '@hcaptcha/react-hcaptcha';
import config from "../config.json"
import ModuleList from "../Components/ModuleList";
import {Typography} from "@material-ui/core";
import {makeStyles} from '@material-ui/core/styles';
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";
import Container from "@material-ui/core/Container";
import RegistryForm from "../Components/RegistryForm";
import axios from "axios";
import Alert from '@material-ui/lab/Alert';
import {Redirect} from "react-router-dom";
import Link from "@material-ui/core/Link";
import InfoColors from "../Components/InfoColors";

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(2),
            width: 200,
        },
    },
    card: {
        minWidth: 300
    },
    padded: {
    }
}));

function Reservation() {
    const classes = useStyles();
    const [erreur, setErreur] = useState("")
    const [succes, setSucces] = useState(false);

    const formSubmit = (e) => {
        e.preventDefault();

        let formData = new FormData(document.querySelector('form'))

        axios
            .post((config.DEBUG ? config.URL_DEV : config.URL_PROD) + '/api/participants/',
                    formData)
            .then((result) => {
                setSucces(true);
                return  <Redirect to="/success/" />
            })
            .catch((err) => {
                console.log("erreur")
                console.log(err);
                setErreur(JSON.stringify(err.response.data));
            });
    }

    return (
        <React.Fragment>
            {succes && <Redirect to={'/success/'}/>}
            <Typography variant='h2'>
                Inscription aux modules du WEV<span style={{color: 'red'}}>A</span>
            </Typography>

            <Container maxWidth="md">
            <Card className={classes.card} variant="outlined">
                {erreur && <Alert severity="error">{erreur}</Alert>}

                <CardContent>
                    <form className={classes.root} onSubmit={formSubmit}>

                        <Typography variant="h3" className="padded">Informations générales</Typography>
                        <RegistryForm/>

                        <Typography variant="h3" className="padded">Choix des modules</Typography>
                        <Typography variant="caption">Toute inscription sera modifiable ultérieurement dans le menu <Link href='/planning'>voir mon planning</Link></Typography>
                        <InfoColors />

                        <ModuleList selected={[]} modif={true}/>

                        <HCaptcha sitekey={config.HCAPTCHA_PUBLIC_KEY}/>

                        {erreur && <Alert severity="error">{erreur}</Alert>}

                        <input type="submit" value="Submit"/>
                    </form>

                    <Typography>NB: Votre absence ne sera pas excusée si vous choisissez de participer à un module pendant une évaluation</Typography>
                </CardContent>

            </Card>
            </Container>
        </React.Fragment>
    )
}

export default Reservation;