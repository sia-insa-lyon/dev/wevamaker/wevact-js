import React from 'react';
import {Button, Container, Typography} from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import FacebookIcon from '@material-ui/icons/Facebook';
import { loadCSS } from 'fg-loadcss';
import Icon from "@material-ui/core/Icon";

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    spaced: {
        margin: theme.spacing(3)
    }
}));

function Home () {
    const classes = useStyles();

    React.useEffect(() => {
        const node = loadCSS(
            'https://use.fontawesome.com/releases/v5.12.0/css/all.css',
            document.querySelector('#font-awesome-css'),
        );

        return () => {
            node.parentNode.removeChild(node);
        };
    }, []);


    return (
        <React.Fragment>
            <Container maxWidth='xl' className={classes.root}>

            <div className={classes.spaced}>
                <Typography variant="h1">WEV<span style={{color: 'red'}}>A</span></Typography>
                <Typography variant="h3">Du 9 au 11 Octobre</Typography>
            </div>

            <Button variant="contained" color="primary" href='/reservation'>M'inscrire</Button>
            <Button variant="contained" color="secondary" href='/planning'>Voir mon planning</Button>
            <Button variant="contained" href='/dashboard'>Places disponibles</Button>

            <div className={classes.spaced}>
                <Typography variant="body1">Plus d'informations sur le déroulement de l'événement sur nos réseaux sociaux</Typography>
                <a href="https://www.facebook.com/events/345027026851956"><FacebookIcon style={{fontSize: 70, padding:10}}/></a>
                <a href=" https://discord.gg/aQYkCDe"><Icon className="fab fa-discord" style={{fontSize: 60, padding:10}}/></a>
            </div>
        </Container>
        </React.Fragment>
    );
}

export default Home;