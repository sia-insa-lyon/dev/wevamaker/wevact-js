import React, {useEffect, useState} from 'react';
import axios from "axios";
import getDomain from "../utils";

function Dashboard() {
    const [moduleList, setModuleList] = useState([]);

    useEffect(() => {
        axios
            .get(getDomain() + "/api/modules/")
            .then(res => {
                setModuleList(res.data);
            })
            .catch(err => console.log(err));
    }, []);

    return (
        <React.Fragment>
            <h1>Places disponibles sur les différents modules</h1>
            <div>
                {moduleList.map(module =>
                    <p>{module.intitule} - {module.places_max - module.places_prises}
                    {(module.places_max - module.places_prises) !== 1 ? " places restantes" : " place restante"}</p>)}
            </div>
        </React.Fragment>
    )
}

export default Dashboard;