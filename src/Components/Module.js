import React from "react";
import Radio from "@material-ui/core/Radio";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import {CardHeader} from "@material-ui/core";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";
import {makeStyles} from "@material-ui/core/styles";
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';

const useStyles = makeStyles((theme) => ({
    formUnit: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
    },
    card: {
        minWidth: 300
    },
}));

function Module({module, modif, selected}) {
    const classes = useStyles()
    const colors = {
        'Vert': "#009933",
        'Jaune': "#ffff00",
        'Noir': "#000000",
        'Bleu': "#3333ff",
    }

    return (
        <FormControlLabel
            value={String(module.pk)}
            control={<Radio id={"creneau" + module.pk}/>}
            label={
                <div className={classes.formUnit}>
                <Card style={selected ? {backgroundColor:'yellow'}:{}}>
                    <CardHeader
                        component='h6'
                        title={<><FiberManualRecordIcon style={{color: colors[module.couleur]}} />{module.intitule}</>}
                        subheader={<><b>{module.intervenant}</b><br/>{String(module.places_max-module.places_prises) + "/" + String(module.places_max) + " places disponibles"}</>}
                    />

                    <CardContent>
                        <div style={{textAlign: "left"}}>
                            {module.description}
                        </div>
                    </CardContent>
                </Card>
                </div>
            }
            disabled={!modif || (module.places_max - module.places_prises) <= 0}
        />
    )
}


export default Module;
