import React from "react";
import {Box, Link, Typography} from "@material-ui/core";

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
           Copyright © Louis Gombert, <Link color="inherit" href="https://sia.asso-insa-lyon.fr/">SIA INSA Lyon</Link>
            {' ' + new Date().getFullYear()}
        </Typography>
    );
}

function Footer() {
    return (<footer><Box mt={8}><Copyright/><br/></Box></footer>);
}

export default Footer;
