import React from "react";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import {makeStyles} from "@material-ui/core/styles";
import {Typography} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    formUnit: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
    },
}));

function RegistryForm() {
    const classes = useStyles();

    return (
        <>
            <div className={classes.formUnit}>
                <FormControl component="fieldset">
                    <FormControlLabel
                        control={<TextField variant="filled"
                                            name="nom"
                                            required={true}
                                            label="Dupont"
                                            size="small"
                        />}
                        label="Nom"
                        labelPlacement="start"/>
                </FormControl>

                <FormControl component="fieldset">
                    <FormControlLabel
                        control={<TextField variant="filled"
                                            name="prenom"
                                            required={true}
                                            label="Jean"
                                            size="small"
                        />}
                        label="Prenom"
                        labelPlacement="start"/>
                </FormControl>
            </div>

            <div className={classes.formUnit}>
                <FormControl component="fieldset">
                    <FormControlLabel
                        control={<TextField variant="filled"
                                            name="mail"
                                            required={true}
                                            label="jean.dupont@insa-lyon.fr"
                                            size="small"
                        />}
                        label="Mail"
                        labelPlacement="start"/>
                </FormControl>

                <FormControl component="fieldset">
                    <FormControlLabel
                        control={<TextField variant="filled"
                                            name="tel"
                                            label="06 14 77 ..."
                                            size="small"
                        />}
                        label="Téléphone"
                        labelPlacement="start"/>
                </FormControl>
            </div>

            <div className={classes.formUnit}>
                <FormControl component="fieldset">
                    <FormControlLabel
                        control={<TextField variant="filled"
                                            name="num_etudiant"
                                            required={true}
                                            label="0401..."
                                            size="small"
                        />}
                        label={<>Numéro étudiant <Typography variant="caption">ou un code à 5 chiffres le cas échéant</Typography></>}
                        labelPlacement="start"/>
                </FormControl>
            </div>

            <div className={classes.formUnit}>
                <FormControl component="fieldset">
                    <FormControlLabel
                        control={<TextField variant="filled"
                                            name="assos"
                                            label="trésorier 24h, ..."
                                            size="small"
                        />
                        }
                        label="Asso(s) représentée(s) et responsabilité(s):"
                        labelPlacement="start"
                    />
                </FormControl>
            </div>

            <div className={classes.formUnit}>
                <FormControl>
                    <FormControlLabel
                        control={
                            <Select
                                id="annee_etude"
                                name="annee_etude"
                                variant="filled"
                                size="small"
                                defaultValue={"1A"}
                            >
                                <MenuItem value={"1A"}>1A</MenuItem>
                                <MenuItem value={"2A"}>2A</MenuItem>
                                <MenuItem value={"3A"}>3A</MenuItem>
                                <MenuItem value={"4A"}>4A</MenuItem>
                                <MenuItem value={"5A"}>5A</MenuItem>
                                <MenuItem value={"6A+"}>6A+</MenuItem>
                                <MenuItem value={"Autre"}>Autre</MenuItem>
                            </Select>
                        }
                        label="Année d'étude "
                        labelPlacement="start"
                    />
                </FormControl>

                <FormControl>
                    <FormControlLabel
                        control={
                            <Select
                                id="departement"
                                name="departement"
                                variant="filled"
                                size="small"
                                defaultValue={"FIMI"}
                            >
                                <MenuItem value={"BS"}>BS</MenuItem>
                                <MenuItem value={"GE"}>GE</MenuItem>
                                <MenuItem value={"GI"}>GI</MenuItem>
                                <MenuItem value={"GCU"}>GCU</MenuItem>
                                <MenuItem value={"GM"}>GM</MenuItem>
                                <MenuItem value={"SGM"}>SGM</MenuItem>
                                <MenuItem value={"TC"}>TC</MenuItem>
                                <MenuItem value={"IF"}>IF</MenuItem>
                                <MenuItem value={"GEN"}>GEN</MenuItem>
                                <MenuItem value={"FIMI"}>FIMI</MenuItem>
                                <MenuItem value={"Autre"}>Autre</MenuItem>
                            </Select>
                        }
                        label="Département "
                        labelPlacement="start"
                    />
                </FormControl>
            </div>
        </>
    )
}


export default RegistryForm;
